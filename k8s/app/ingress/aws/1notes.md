# configs

```
data:
  allow-snippet-annotations: "true"
  http-snippet: |
    server {
      listen 2443;
      return 308 https://$host$request_uri;
    }
  proxy-real-ip-cidr: XXX.XXX.XXX/XX
  use-forwarded-headers: "true"
```

```
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout: "60"
    service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: "true"
    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: arn:aws:acm:us-west-2:XXXXXXXX:certificate/XXXXXX-XXXXXXX-XXXXXXX-XXXXXXXX
    service.beta.kubernetes.io/aws-load-balancer-ssl-ports: https
    service.beta.kubernetes.io/aws-load-balancer-type: nlb -> Application Load Balancer or Network Load Balancer
```
