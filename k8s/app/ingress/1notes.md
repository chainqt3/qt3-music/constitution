Ingress docs
```
https://kubernetes.github.io/ingress-nginx/
```

### install nginx-ingress-controller using helm from bitnami
```
helm repo add my-repo https://charts.bitnami.com/bitnami
helm install my-release my-repo/nginx-ingress-controller
```

### install from docker hub
```
docker pull bitnami/nginx-ingress-controller
docker pull rancher/nginx-ingress-controller
```

### installing using https://kubernetes.github.io/ingress-nginx/deploy/
```
# if error occur
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
  
Release "ingress-nginx" does not exist. Installing it now.
NAME: ingress-nginx
LAST DEPLOYED: Sun Dec 11 16:55:30 2022
NAMESPACE: ingress-nginx
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running 'kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller'

An example Ingress that makes use of the controller:
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: example
    namespace: foo
  spec:
    ingressClassName: nginx
    rules:
      - host: www.example.com
        http:
          paths:
            - pathType: Prefix
              backend:
                service:
                  name: exampleService
                  port:
                    number: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
      - hosts:
        - www.example.com
        secretName: example-tls

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls
```

```
kubectl get all -n ingress-nginx

```

### Configure kubernetes-dashboard in minikube
```
kubectl apply -f kubernetes-dashboard-ingress.yml
sudo nano /etc/hosts -> <address name endpoint> chainqt3.dashboard.com
```

### Generate tls cert
```
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls.key -out tls.crt -subj "/CN=chainqt3.dashboard.com" -days 365
kubectl create secret tls kubernetes-dashboard-tls-secret --cert=tls.crt --key=tls.key -n kubernetes-dashboard

# Encode/Decode
echo -n password | base64
echo -n cGFzc3dvcmQ= | base64 --decode
cat tls.crt | base64
cat tls.key | base64
```
