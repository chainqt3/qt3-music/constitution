### notes
```
kubectl port-forward service/discovery-server 8761
kubectl port-forward service/gateway 8000

kubectl port-forward service/artist-service 7002
kubectl port-forward service/user-service 7000

```

### pull new image and restart deployment
```
minikube image pull ...
kubectl rollout restart deployment/
```
