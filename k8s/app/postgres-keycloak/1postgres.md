```
kubectl apply -f persistentvolume.yml
kubectl apply -f persistentvolumeclaim.yml
kubectl apply -f postgres.yml

kubectl apply -f persistentvolume.yml && \
kubectl apply -f persistentvolumeclaim.yml && \
kubectl apply -f postgres.yml
kubectl apply -f keycloak.yml

```

```
kubectl exec -it pod/postgres-kc-0 bash
psql --username=admin postgres
psql --username=keycloak postgres
ALTER USER keycloak WITH PASSWORD 'password1';
\l
\q
```

```
kubectl get configmap postgres-config -o json
kubectl delete pvc keycloak-claim && kubectl delete pv keycloak-volume
```
