### creates clusterrolebinding named service-reader-pod using clusterrole service-reader for default service account
```
kubectl create clusterrolebinding service-reader-pod \
  --clusterrole=service-reader  \
  --serviceaccount=default:default
```
