https://www.keycloak.org/getting-started/getting-started-kube
https://www.itwonderlab.com/en/postgres-kubernetes-nfs-volume/
https://blog.knoldus.com/how-to-deploy-keycloak-with-postgres-on-gke/
https://github.com/lukaszbudnik/keycloak-kubernetes
https://medium.com/@ManagedKube/kubernetes-troubleshooting-ingress-and-services-traffic-flows-547ea867b120

### Keycloak
```
minikube start
kubectl create -f keycloak.yml
kubectl apply -f keycloak.yml

KEYCLOAK_URL=https://keycloak.$(minikube ip).nip.io &&
echo "" &&
echo "Keycloak:                 $KEYCLOAK_URL" &&
echo "Keycloak Admin Console:   $KEYCLOAK_URL/admin" &&
echo "Keycloak Account Console: $KEYCLOAK_URL/realms/myrealm/account" &&
echo ""

```
### postgres
```
kubectl create -f postgres-keycloak.yml
Check PersistantVolumes
kubectl get pv
```

### Cleanup kubernetes
```
kubectl delete all --all
kubectl delete pod keycloak-8689459db6-t2slp
kubectl delete pod keycloak-8689459db6-lwxtv

Watch pods
kubectl get pods -w

kubectl get pods
kubectl exec -it <pod-name> -c <container????> bash

decode
echo YWRtaW4K | base64 --decode
```

### Calling pod in cluster
```
kubectl get services
minikube service <service-name>
# list images
minikube image ls 
```

### Check metrics
```
kubectl top pod
kubectl top pod --namespace=kube-system
```

### Gitlab-authentication in k8s

```
kubectl create secret docker-registry gitlab-registry \
  --docker-server=registry.gitlab.com \
  --docker-username=your-username \
  --docker-password=your-token
```



### https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/kubernetes
