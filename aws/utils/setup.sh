#!/bin/bash

# setup
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id) && \
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key) && \
export NAME=chainqt3.com && \
export KOPS_STATE_STORE=s3://chainqt3-com

# create cluster
kops create cluster \
--name=${NAME} \
--cloud=aws \
--zones=us-east-1f \
--discovery-store=s3://chainqt3-com-oidc-store/${NAME}/discovery && \
kops update cluster --name ${NAME} --yes --admin

# keycloak
kubectl apply -f persistentvolume.yml && \
kubectl apply -f persistentvolumeclaim.yml && \
kubectl apply -f postgres.yml && \
kubectl apply -f keycloak.yml
# setup postgres to work with keycloak
kubectl exec -it pod/postgres-kc-0 bash
psql --username=keycloak postgres
ALTER USER keycloak WITH PASSWORD 'password1';

# services
kubectl apply -f config-server.yml && \
kubectl apply -f discovery-server.yml && \
kubectl apply -f gateway.yml && \
kubectl apply -f user-service.yml && \
kubectl apply -f artist-service.yml

# ingress






# Delete resources from aws
kops delete cluster --name ${NAME} --yes
