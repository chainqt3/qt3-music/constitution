# Notes

### Spring Cloud Architecture

![springcloud](https://images.chaincuet.com/wiki/springcloud.png)

### Kubernetes Architecture

![kubernetes](https://images.chaincuet.com/wiki/kubernetes.png)

### TODO

```
Kubernetes production setup (Ingress, nginx-ingress-controller, alb, ec2 - keycloak)

promethues - grafana
TestContainers
Messaging (kafka) Message Brokers
Redis cache

Documentation made easy
https://squidfunk.github.io/mkdocs-material/getting-started/
```
