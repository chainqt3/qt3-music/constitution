#!/bin/bash

#docker-compose -f docker-compose-dev.yml up -d --force-recreate
#docker-compose -f docker-compose-metrics.yml up -d --force-recreate
#docker-compose -f docker-compose-services.yml up -d --force-recreate

docker-compose -f docker-compose-dev.yml up -d
docker-compose -f docker-compose-metrics.yml up -d
docker-compose -f docker-compose-services.yml up -d

#docker-compose -f docker-compose-portals.yml up -d
