```
docker-compose -f docker-compose-dev.yml up --build -d
```


```
docker-compose -f docker-compose-kc.yml up --build -d
```

```
./start-dev.sh
```

### setup keycloak
```
{
    "realm": "qt3music",
    "auth-server-url": "http://localhost:8080/auth/",
    "ssl-required": "external",
    "resource": "qt3music-client",
    "public-client": true,
    "verify-token-audience": true,
    "use-resource-role-mappings": true,
    "confidential-port": 0
}

realm: qt3music
client: qt3music-client -> Valid redirect URIs=http://localhost:3000* -> Web origins=*
add role 'user' in client
create role user
create user u@gmail.com, add role user
```
